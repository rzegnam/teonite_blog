# teonite_blog
```
REQUIREMENTS:
scrapy
pymongo
django
mongoengine
django_rest_framework
django-rest-framework-mongoengine
```

# Running spider
```
cd to 'teonite' directory then run the command: 'scrapy crawl build'
```

# API:
```
http://127.0.0.1:8000/
http://127.0.0.1:8000/stats/
http://127.0.0.1:8000/stats/<author>/
```
