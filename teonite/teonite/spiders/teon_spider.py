import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join, Compose
from teonite.items import TeoItem



class MyItemLoader(ItemLoader):
    default_item_class = TeoItem

    author_in=Compose(lambda v: [x.replace(" ", "") for x in v], lambda v: [x.lower() for x in v])
    paragraph_in=Compose(lambda v: [x.lower() for x in v])



class TeonSpider(scrapy.Spider):
    name = "build"
    start_urls = [
        'https://teonite.com/blog',
    ]

    def parse(self, response):
        for content in response.css('main.content'):
            l = MyItemLoader(response=response)
            l.add_css('author', 'span.author-content h4::text')
            l.add_xpath('paragraph', '//section[@class="post-content"]/p//text()')
            yield l.load_item()

        if response.url == 'https://teonite.com/blog/':
            next_page = response.css('h2.post-title a::attr(href)').extract_first()
        else:
            next_page = response.css('li.pull-left a::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)