# -*- coding: utf-8 -*-
# https://stackoverflow.com/questions/20510768/python-count-frequency-of-words-in-a-list
from __future__ import unicode_literals

from django.shortcuts import render
from .models import Post
from .serializers import PostSerializer

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from collections import Counter

import re


class PostListView(APIView):
    def get(self, request, format=None):
        all_posts = Post.objects.all()
        serializer = PostSerializer(all_posts, many=True)
        return Response(serializer.data)

class StatsView(APIView):
    def get(self, request, format=None):
        all_posts = Post.objects.all()
        text = []
        for post in all_posts:
            text.append(post.paragraph)

        text = re.findall(r'\w+\b(?<!\bu)', str(text))
        return Response(Counter(text).most_common(10))

class AuthorStatsView(APIView):
    def get(self, request, author_name):
        author = Post.objects.filter(author = author_name)
        text = []
        for name in author:
            text.append(name.paragraph)

        text = re.findall(r'\w+\b(?<!\bu)', str(text))
        return Response(Counter(text).most_common(10))
